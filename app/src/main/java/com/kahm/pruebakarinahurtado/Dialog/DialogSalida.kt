package com.kahm.pruebakarinahurtado.Dialog

import android.app.Activity
import android.os.Bundle
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.kahm.pruebakarinahurtado.R


class DialogSalida : DialogFragment(),  View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view =
            inflater.inflate(R.layout.dialog_fragment_salida,container,false)

        var cancelButton = view.findViewById<Button>(R.id.cancelSalir)
        var confirmarButton = view.findViewById<Button>(R.id.confirmarSalir)

        cancelButton.setOnClickListener(this)
        confirmarButton.setOnClickListener(this)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.confirmarSalir->{
                System.exit(0)
            }
            R.id.calculoMonedas->{
                Toast.makeText(activity!!.applicationContext,
                    android.R.string.no, Toast.LENGTH_SHORT).show()

            }
        }
    }
}
