package com.kahm.pruebakarinahurtado.Dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kahm.pruebakarinahurtado.Adapter.MovimientosAdapter
import com.kahm.pruebakarinahurtado.R
import com.kahm.pruebakarinahurtado.bean.ValorIntercambio
import com.kahm.pruebakarinahurtado.bean.ValoresIntercambio


class DialogDetalleTransaccion : DialogFragment(){

    internal lateinit var listaMovimientos: ArrayList<ValoresIntercambio>
    internal lateinit var rcMovi: RecyclerView
    var monto: String = ""
    var moneda : String = ""
    var infoQr : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view =
            inflater.inflate(R.layout.dialog_detalle_transac, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rcMovi = view.findViewById(R.id.rcvdetalle)
        rcMovi.layoutManager = LinearLayoutManager(context)
        consultarTransacciones()
        var adapter = MovimientosAdapter(listaMovimientos)
        rcMovi.adapter = adapter
        if (getArguments() != null) {
            val mArgs = arguments
            monto= mArgs!!.getString("monto")!!
            moneda= mArgs!!.getString("moneda")!!

        }

    }

    private fun consultarTransacciones() {

        var movimientos: ValoresIntercambio? = null
        listaMovimientos = ArrayList<ValoresIntercambio>()

           movimientos = ValoresIntercambio()
           movimientos.Moneda = moneda
           movimientos.Monto = monto
           movimientos.infoQr ="{'monto':'${monto}','moneda':'${moneda}'}"

           listaMovimientos.add(movimientos)




    }

}