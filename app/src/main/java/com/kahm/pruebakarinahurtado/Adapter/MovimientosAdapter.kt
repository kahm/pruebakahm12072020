package com.kahm.pruebakarinahurtado.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView
import com.kahm.pruebakarinahurtado.R
import com.kahm.pruebakarinahurtado.bean.ValoresIntercambio


import java.util.ArrayList

class MovimientosAdapter(internal var listaMovimientos: ArrayList<ValoresIntercambio>) :
    RecyclerView.Adapter<MovimientosAdapter.MovimientosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovimientosViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.detalle_transaccion, null, false)
        return MovimientosViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovimientosViewHolder, position: Int) {

        holder.txtMonto.text = listaMovimientos[position].monto.toString()
        holder.txtMoneda.text = listaMovimientos[position].moneda
        //Log.d("status", holder.txtInfoQr.text as String?)
         holder.imagen.setImageResource(R.drawable.qr24)

    }

    override fun getItemCount(): Int {
        return listaMovimientos.size
    }

    inner class MovimientosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        internal var txtMonto: TextView
        internal var txtMoneda: TextView
        internal var imagen :  ImageView

        init {

            txtMonto = itemView.findViewById<View>(R.id.txtMonto) as TextView
            txtMoneda = itemView.findViewById<View>(R.id.txtMoneda) as TextView
            imagen = itemView.findViewById<View>(R.id.qrInfo) as ImageView
        }
    }
}

