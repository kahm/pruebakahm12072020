package com.kahm.pruebakarinahurtado.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

import androidx.recyclerview.widget.RecyclerView
import com.kahm.pruebakarinahurtado.R
import com.kahm.pruebakarinahurtado.bean.ValoresIntercambio

import java.util.ArrayList

class ListaMovimientosAdapter(internal var listaMovimientos: ArrayList<ValoresIntercambio>,
                              internal var onListener : onTransactionClick) :
    RecyclerView.Adapter<ListaMovimientosAdapter.MovimientosViewHolder>(){


    var onListener2 : onTransactionClick = onListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovimientosViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.vista_calculo, null, false)
        return MovimientosViewHolder(view, onListener2)
    }

    override fun onBindViewHolder(holder: MovimientosViewHolder, position: Int) {
        holder.txtMonto.text = listaMovimientos[position].Monto
        holder.txtMoneda.text = listaMovimientos[position].Moneda


    }


    override fun getItemCount(): Int {
        return listaMovimientos.size
    }

    class MovimientosViewHolder(itemView: View, onListener : onTransactionClick) : RecyclerView.ViewHolder(itemView),
    View.OnClickListener{

        internal var txtMonto: TextView
        internal var txtMoneda: TextView
        val onListener2 = onListener
        init {
            txtMonto = itemView.findViewById<View>(R.id.txtMonto) as TextView
            txtMoneda = itemView.findViewById<View>(R.id.txtMoneda) as TextView

            txtMonto.setOnClickListener(this)
            txtMoneda.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            onListener2.onClickTransac(adapterPosition)
            when(v!!.id){
                R.id.txtMoneda->{

                }
            }
        }
    }


    interface onTransactionClick{
        fun onClickTransac(position : Int)
    }
}



