package com.kahm.pruebakarinahurtado.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ValorIntercambio {

    @SerializedName("moneda")
    @Expose
    private String moneda;
    @SerializedName("monto")
    @Expose
    private Float monto;

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public Float getMonto() {
        return monto;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }

}