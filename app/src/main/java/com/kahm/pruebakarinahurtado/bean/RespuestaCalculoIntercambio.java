package com.kahm.pruebakarinahurtado.bean;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RespuestaCalculoIntercambio {

    @SerializedName("valorIntercambio")
    @Expose
    private List<ValorIntercambio> valorIntercambio = null;

    public List<ValorIntercambio> getValorIntercambio() {
        return valorIntercambio;
    }

    public void setValorIntercambio(List<ValorIntercambio> valorIntercambio) {
        this.valorIntercambio = valorIntercambio;
    }

}