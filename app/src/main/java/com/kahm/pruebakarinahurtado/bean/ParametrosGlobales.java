package com.kahm.pruebakarinahurtado.bean;

import java.util.List;

public class ParametrosGlobales {

    public static String token = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJJVCBTZWN1cml0eSBTb2x1dGlvbiBDLkEiLCJpYXQiOjE1ODYzMTU1MzMsImV4cCI6MTU4NjMxNjQzMywiZW1haWwiOiJrYXJpbmEuaHVydGFkb0BpdHNzY2EubmV0IiwiY29tZXJjaW8iOiJJVCBTZWN1cml0eSBTb2x1dGlvbiBDLkEiLCJsaWNlbmNpYSI6IjJqaHVmbDc0Z2ZqazQ4NDg5cmY0OCJ9.upPmaWHYSrQqV_OqqxAJ46GH56HANy4PhzErpy9go7A";
    public static String login = "Karina Hurtado";

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        ParametrosGlobales.token = token;
    }

    public static String getLogin() {
        return login;
    }

    public static void setLogin(String login) {
        ParametrosGlobales.login = login;
    }
}
