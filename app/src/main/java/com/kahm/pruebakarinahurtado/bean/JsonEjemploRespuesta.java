package com.kahm.pruebakarinahurtado.bean;

public class JsonEjemploRespuesta {

    String jsonIntercambio = "{\n" +
            "    \"valorIntercambio\": [\n" +
            "        {\n" +
            "            \"moneda\": \"BS\",\n" +
            "            \"monto\": 100000.00\n" +
            "        },\n" +
            "        {\n" +
            "            \"moneda\": \"USD\",\n" +
            "            \"monto\": 5.0\n" +
            "        },\n" +
            "        {\n" +
            "            \"moneda\": \"EUR\",\n" +
            "            \"monto\": 4.0\n" +
            "        },\n" +
            "        {\n" +
            "            \"moneda\": \"ETH\",\n" +
            "            \"monto\": 0.01\n" +
            "        },\n" +
            "        {\n" +
            "            \"moneda\": \"BTC\",\n" +
            "            \"monto\": 0.02\n" +
            "        },\n" +
            "        {\n" +
            "            \"moneda\": \"PTR\",\n" +
            "            \"monto\": 0.01\n" +
            "        }\n" +
            "    ]\n" +
            "}";

    public String getJsonIntercambio() {
        return jsonIntercambio;
    }
}
