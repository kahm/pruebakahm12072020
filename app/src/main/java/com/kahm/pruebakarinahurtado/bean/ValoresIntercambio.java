package com.kahm.pruebakarinahurtado.bean;

public class ValoresIntercambio {
    public String Moneda;
    public String Monto;
    public String infoQr;

    public String getMoneda() {
        return Moneda;
    }



    public void setMoneda(String moneda) {
        Moneda = moneda;
    }

    public String getMonto() {
        return Monto;
    }

    public void setMonto(String monto) {
        Monto = monto;
    }

    public String getInfoQr() {
        return infoQr;
    }

    public void setInfoQr(String infoQr) {
        this.infoQr = infoQr;
    }
}
