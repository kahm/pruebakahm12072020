package com.kahm.pruebakarinahurtado.Principal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kahm.pruebakarinahurtado.R

class Principal : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_principal)
    }
}
