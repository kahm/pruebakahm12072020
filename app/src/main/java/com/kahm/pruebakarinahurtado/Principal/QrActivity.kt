package com.kahm.pruebakarinahurtado.Principal

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.navOptions
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder

import com.kahm.pruebakarinahurtado.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [QrActivity.newInstance] factory method to
 * create an instance of this fragment.
 */
class QrActivity : Fragment(), View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    var navController: NavController? = null
    internal lateinit var btnAceptar: Button
    internal lateinit var btnCaptura: Button
    internal lateinit var etMonto: EditText
    internal lateinit var Qr: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qr_activity, container, false)
    }

    override fun onViewCreated(view : View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        navController = Navigation.findNavController(view)
        btnAceptar = view.findViewById(R.id.AceptarPayQr)
        btnCaptura = view.findViewById(R.id.btnCapturaQr)
        Qr = view.findViewById(R.id.qrCaptura)
        etMonto = view.findViewById(R.id.montoQr)

        btnAceptar.setOnClickListener(this)
        btnCaptura.setOnClickListener(this)


    }

    private fun generarQr() {

        try {
            val barcodeEncoder = BarcodeEncoder()
            val bitmap =
                barcodeEncoder.encodeBitmap(etMonto.text.toString(), BarcodeFormat.QR_CODE, 550, 550)
            Qr.setImageBitmap(bitmap)
            //generateQRCodeEMV()
        } catch (e: Exception) {
            Log.e("Error: ", "${e.toString()}")
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment QrActivity.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            QrActivity().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        val options = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
        }
        when(v!!.id){
            R.id.AceptarPayQr->{
                generarQr()
            }
            R.id.btnCapturaQr->{
                navController!!.navigate(R.id.action_qrActivity_to_leerQr,
                    null, options)
            }
        }
    }
}
