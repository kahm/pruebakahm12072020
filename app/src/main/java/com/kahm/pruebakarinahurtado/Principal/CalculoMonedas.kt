package com.kahm.pruebakarinahurtado.Principal

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.kahm.pruebakarinahurtado.Adapter.ListaMovimientosAdapter
import com.kahm.pruebakarinahurtado.Adapter.MovimientosAdapter
import com.kahm.pruebakarinahurtado.Dialog.DialogDetalleTransaccion

import com.kahm.pruebakarinahurtado.R
import com.kahm.pruebakarinahurtado.bean.JsonEjemploRespuesta
import com.kahm.pruebakarinahurtado.bean.RespuestaCalculoIntercambio
import com.kahm.pruebakarinahurtado.bean.ValoresIntercambio
import com.kahm.pruebakarinahurtado.conexion.ConexionAPI
import kotlinx.android.synthetic.main.fragment_calculo_monedas.*
import java.text.ParseException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
var navController: NavController? = null

/**
 * A simple [Fragment] subclass.
 * Use the [CalculoMonedas.newInstance] factory method to
 * create an instance of this fragment.
 */
class CalculoMonedas : Fragment(), ListaMovimientosAdapter.onTransactionClick, View.OnClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null


    internal lateinit var btnCalcular: Button
    internal lateinit var etValorMoneda : EditText
    internal lateinit var etnombreUsuario : TextView
    internal lateinit var spMoneda :Spinner
    internal lateinit var listaMovimientos: ArrayList<ValoresIntercambio>
    internal lateinit var rcMovi: RecyclerView
    internal var tipoMoneda =""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_calculo_monedas, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
        btnCalcular = view.findViewById(R.id.btnCalcular)
        etnombreUsuario = view.findViewById(R.id.nombreUsuario)
        etValorMoneda = view.findViewById(R.id.etmonto)
        spMoneda = view.findViewById(R.id.SpinnerMonedas)

        btnCalcular.setOnClickListener(this)

        rcMovi = view.findViewById(R.id.rcvdetalle)
        rcMovi.layoutManager = LinearLayoutManager(context)


        arguments?.let {
            val nombreUser: String = getArguments()?.getString("nombre").toString()
            etnombreUsuario.setText(nombreUser)
        }

    }

    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CalculoMonedas().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.btnCalcular->{
                //iniciarCalculo()
                consultarTransacciones()
            }
        }
    }

    private fun iniciarCalculo() {
        val dialogFragment = DialogDetalleTransaccion()
        val bundle = Bundle()
        dialogFragment.arguments = bundle
        fragmentManager?.let { dialogFragment.show(it,"transaccion") }
    }

    private fun consultarTransacciones() {

        var movimientos: ValoresIntercambio? = null
        listaMovimientos = ArrayList<ValoresIntercambio>()
        var  respuestaApi = JsonEjemploRespuesta().jsonIntercambio
        var gson = Gson()
        val respuestaCalculo: RespuestaCalculoIntercambio
        respuestaCalculo = gson.fromJson(respuestaApi, RespuestaCalculoIntercambio::class.java)
        for (i in respuestaCalculo.valorIntercambio.indices) {
            movimientos = ValoresIntercambio()
            movimientos.Moneda = respuestaCalculo.valorIntercambio[i].moneda
            movimientos.Monto = "${respuestaCalculo.valorIntercambio[i].monto}"

            listaMovimientos.add(movimientos)
        }

        var adapter = ListaMovimientosAdapter(listaMovimientos, this)
        rcMovi.adapter = adapter
    }


    override fun onClickTransac(position: Int) {
        Log.d("position Click",position.toString())
        val dialogFragment = DialogDetalleTransaccion()
        val bundle = Bundle()
        bundle.putString("moneda", listaMovimientos[position].moneda)
        bundle.putString("monto", listaMovimientos[position].monto)
        dialogFragment.arguments = bundle
        fragmentManager?.let { dialogFragment.show(it,"transaccion") }

    }

    private fun opcMoneda() {
        spMoneda.setOnItemSelectedListener(
            object  : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if((parent!!.getItemAtPosition(position)=="USD")){
                        tipoMoneda =parent.getItemAtPosition(position).toString()

                    }else if(parent.getItemAtPosition(position)=="BS"){
                        tipoMoneda ="USD"
                    }else if(parent.getItemAtPosition(position)=="EUR"){
                        tipoMoneda ="EUR"

                    }else if(parent.getItemAtPosition(position)=="PTR"){
                        tipoMoneda ="PTR"

                    }else if(parent.getItemAtPosition(position)=="BTC"){
                        tipoMoneda ="BTC"

                    }else if(parent.getItemAtPosition(position)=="ETC"){
                        tipoMoneda =parent.getItemAtPosition(position).toString()

                    }else{

                    }
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

            }
        )
    }

    private fun peticionCalculo() {
        Log.d("pidiendo", "moneda")
        ConexionAPI(activity!!.applicationContext).ConsultaCalculo(
            "${etmonto.text}", "${tipoMoneda}", callbackExito = { Json -> }
            , callbackFallo = { ParseError ->
                Log.i("errorVolley", ParseError.cause.toString())

            })
    }

}
