package com.kahm.pruebakarinahurtado.Login

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.NavController
import androidx.navigation.Navigation

import com.kahm.pruebakarinahurtado.R
import com.kahm.pruebakarinahurtado.bean.ParametrosGlobales

class LoginFragment : Fragment() , View.OnClickListener{

    var navController : NavController? = null
    internal lateinit var btnEntrar: Button
    internal lateinit var edtUser: EditText
    internal lateinit var edtPass: EditText
    internal lateinit var progressView : View
    private val DURACION_SPLASH = 1500 // 1.5 segundos

    companion object {
        fun newInstance() = LoginFragment()
    }

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)
        btnEntrar = view.findViewById(R.id.botonIniciarSesion)
        edtUser  = view.findViewById(R.id.usuarioFieldLogin)
        edtPass  = view.findViewById(R.id.passwordFieldLogin)
        progressView = view.findViewById(R.id.progress_login2)
        btnEntrar.setOnClickListener(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onClick(v: View?) {
        when(v!!.id) {
            R.id.botonIniciarSesion -> {
                progressView.visibility = View.VISIBLE
                if (edtUser.text.toString().equals("kahm") && edtPass.text.toString()
                        .equals("admin")
                ) {
                    edtUser.setText("")
                    edtPass.setText("")
                    Handler().postDelayed({
                        progressView.visibility =View.GONE
                        val bundle = Bundle()
                        bundle.putString("nombre", "${ParametrosGlobales.login}")
                        navController!!.navigate(R.id.action_loginFragment2_to_mainActivity, bundle)
                    }, DURACION_SPLASH.toLong())


                } else {

                    Handler().postDelayed({
                        progressView.visibility =View.GONE
                        navController!!.navigate(R.id.action_loginFragment2_to_errorLogin)
                    }, DURACION_SPLASH.toLong())

                }
            }
        }
    }

}
