package com.kahm.pruebakarinahurtado.Login

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kahm.pruebakarinahurtado.R

class ErrorLogin : Fragment() {

    companion object {
        fun newInstance() = ErrorLogin()
    }

    private lateinit var viewModel: ErrorLoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.error_login_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ErrorLoginViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
