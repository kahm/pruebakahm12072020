package com.kahm.pruebakarinahurtado.conexion

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

/**
 * Singleton usado para: 1. Manejar una única cola de peticiones [RequestQueue] (Recomiendación de guías de Android).
 *                       2. Guardar una lista única por sesión de OTPs generados y enviados para evitar repeticiones.
 */
class ConexionSingletonHelper constructor(context: Context) {
    companion object {
        @Volatile
        private var INSTANCE: ConexionSingletonHelper? = null
        fun getInstance(context: Context) =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: ConexionSingletonHelper(context.applicationContext).also {
                    INSTANCE = it
                }
            }
    }

    val listaOTPs: ArrayList<String> by lazy {
        ArrayList<String>()
    }

    fun cancelarPeticiones(tag: Any){
        requestQueue.cancelAll(tag)
    }

    private val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context.applicationContext)
    }
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue.add(req)
    }
}