package com.kahm.pruebakarinahurtado.conexion

import android.content.Context
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson
import com.kahm.pruebakarinahurtado.R

class ConexionAPI (private val appContext: Context){

    fun  ConsultaCalculo(monto: String, moneda : String, callbackExito: (Json : String) -> Unit,
                         callbackFallo: (error: VolleyError) -> Unit){

        val urlInicial: String = StringBuilder("")
            .append(appContext.getString(R.string.urlcalculo))
            .toString()

        val mensaje : String = "monto=${monto}&moneda=${moneda}"
        Log.d("mensajeGateway", mensaje)


        return  PeticionCalculoIntercambio(urlInicial, mensaje ,TAG, callbackExito = callbackExito, callbackFallo = callbackFallo)

    }

    fun PeticionCalculoIntercambio(url: String, mensajeGateway: String, requestTag: String,
                           callbackExito: (Json :String) -> Unit,
                           callbackFallo: (error: VolleyError) -> Unit){

        var request = Volley.newRequestQueue(appContext)

        val urlString: String = StringBuilder("")
            .append(url)
            .append("/$mensajeGateway")
            .toString()
        Log.i("urlString", urlString)
        val stringRequest= StringRequest(Request.Method.GET,urlString, Response.Listener<String>{
                jsonObject -> //Acción si la solicitud web fue exitosa. Resultado es un JSONObject
            callbackExito(jsonObject)
            Log.d("respuesta",jsonObject)

        },  Response.ErrorListener { error -> error.printStackTrace()
            Log.e("Error  Volley",error.toString())
            callbackFallo(ParseError(error))

        } ).apply {
            //Para configurar el timeout
            retryPolicy = DefaultRetryPolicy(appContext.resources.getInteger(R.integer.timeout_ms), 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            //TAG para poder cancelar peticiones.
            tag = requestTag
        }
        request.add(stringRequest)

    }

    companion object {

        private val TAG: String = ConexionAPI::class.java.simpleName
    }

}